<br>
<center>
  <h4 style="font-weight:bold">Categoria de Accesorio Registrado</h4><br>
</center>
<?php if ($listadoGeneros): ?>
  <table class="table" id="tbl-generos">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">CATEGORIA DE ACCESORIOS</th>
        <th class="text-center">ACCIONES</th>
      </tr>
  </thead>
    <tbody>
      <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $generoTemporal->id_ge ?></td>
          <td class="text-center"><?php echo $generoTemporal->nombre_gen ?></td>
          <td class="text-center">
          <a href="<?php echo site_url(); ?>/generos/editar/<?php echo $generoTemporal->id_ge ?>">
            <i class='fa fa-pencil' title="Editar"></i></a>
          <a href="<?php echo site_url(); ?>/generos/eliminarGenero/<?php echo $generoTemporal->id_ge ?>"
            onclick="confirmation(event)">
            <i class='fa fa-trash-o' title="Eliminar"></i>

          </a>  </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    No se encontraron generos registrados
  </div>
<?php endif; ?>

</script>
<br><br>
<script type="text/javascript">
$(document).ready( function () {
  $('#tbl-generos').DataTable();
} );
</script>

<style media="screen">
thead tr{
  background-color: white !important;
}
  .odd{
    background-color: blue !important;
  }
  .even{
    background-color: green !important;
  }
</style>

<br><br>
