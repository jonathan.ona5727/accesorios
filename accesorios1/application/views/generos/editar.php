<br><br><br><br><br>

<section class="breadcumb-area bg-img bg-overlay" style="background-image: url(https://images5.alphacoders.com/920/thumb-1920-920085.jpg);">
       <div class="bradcumbContent">
           <p></p>
           <h2>Actualizar Genero Musical</h2>
       </div>

   </section>
   <!-- ##### Breadcumb Area End ##### -->

   <!-- ##### Login Area Start ##### -->
   <section class="login-area section-padding-100">
       <div class="container">
           <div class="row justify-content-center">
             <div class="col-md-2">

             </div>
               <div class="col-md-8 ">
                 <div class="col-3">

                 </div>
                   <div class="login-content">
    <form  class="" action="<?php echo site_url()?>/generos/actualizarGenero"
    method="post" id="frm_nuevoGenero">
    <input type="hidden" name="id_ge"  id="id_ge" class="form-control"
    value="<?php echo $generoEditar->id_ge;?>">

    <div class="row">
      <!-- cedula -->
      <div class="col-md-6 text-right">
        <label for="">Nuevo Genero Musical: </label>
      </div>
      <div class="col-md-6 text-left">
        <input type="text" name="nombre_gen"  id="nombre_gen" class="form-control"
        value="<?php echo $generoEditar->nombre_gen;?>"  required autocomplete="off">

      </div>

      <br>
      <br><br>
      <br><br><br>
    </div>
      <button type="submit" name="button" class="btn btn-primary" style="background-color:#17BB0F">
        Guardar
      </button>
      <a href="<?php echo site_url()?>/generos/index"  class="btn btn-danger">
        Cancelar
      </a>
      <br><br><br>
    </form>
  </div>
</div>
</div>
</div>
</section>
<!-- ##### Login Area End ##### -->
    <br>

  </div>
  <div class="col-md-3 text-center" >
  </div>
</div>
<!-- script interpretado como javascript o jquery para que no se guarde en blanco sino que nos marque los campos que faltan de llaner -->
<script type="text/javascript">
$("#frm_nuevoGenero").validate({
  rules:{
    nombre_gen:{
      required:true
    },

  },
  // -----------------Mensajes----------
  messages:{
        nombre_gen:{
          required:"Por favor debe llenar este campo"
        }
      }
    });

</script>
