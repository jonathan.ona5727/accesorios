<!-- ##### Breadcumb Area Start ##### -->
    <section class="breadcumb-area bg-img bg-overlay" style="background-image: url(https://images3.alphacoders.com/266/thumb-1920-266779.jpg);">
        <div class="bradcumbContent">
            <h2>Actualizar cliente</h2>
        </div>
    </section>
    <!-- ##### Breadcumb Area End ##### -->

   <!-- ##### Login Area Start ##### -->
   <section class="login-area section-padding-100">
       <div class="container">
           <div class="row justify-content-center">
               <div class="col-md-8 ">
                   <div class="login-content">
    <form class="" action="<?php echo site_url()?>/clientes/actualizarCliente"
    method="post" id="frm_editarCliente">

    <input type="hidden" name="id_cli"  id="id_cli" class="form-control"
    value="<?php echo $clienteEditar->id_cli;?>">

    <div class="row">
      <!-- cedula -->
      <div class="col-md-4 text-right">
        <label for="">Cedula: </label>
      </div>
      <div class="col-md-8 text-left">
        <input type="number" name="cedula_cli"  id="cedula_cli" class="form-control"
        value="<?php echo $clienteEditar->cedula_cli;?>">
        Ej. 1722541521
      </div>
      <!-- apellidos -->
      <div class="col-md-4 text-right">
        <label for="">Apellidos: </label>
      </div>
      <div class="col-md-8 text-left">
        <input type="apellidos" name="apellidos_cli"  id="apellidos_cli" class="form-control"
        value="<?php echo $clienteEditar->apellidos_cli;?>">
        Ej. Garía Marquez
      </div>
      <br>
      <!-- Nombres -->
      <div class="col-md-4 text-right">
        <label for="">Nombres: </label>
      </div>
      <div class="col-md-8 text-left">
        <input type="nombres" name="nombres_cli"  id="nombres_cli" class="form-control"
        value="<?php echo $clienteEditar->nombres_cli;?>">
        Ej. Juan Carlos
      </div>
      <br>
      <!-- Direccio -->
      <div class="col-md-4 text-right">
        <label for="">Direccion Exacta: </label>
      </div>
      <div class="col-md-8 text-left">
        <input type="direccion" name="direccion_cli"  id="direccion_cli" class="form-control"
        value="<?php echo $clienteEditar->direccion_cli;?>">
        Ej. Barrio El Ejigo,Av.Simón Rodrigez
      </div>
      <br>
      <!-- Telefono celular -->
      <div class="col-md-4 text-right">
        <label for="">Teléfono Celular: </label>
      </div>
      <div class="col-md-8 text-left">
        <input type="number" name="celular_cli"  id="celular_cli" class="form-control"
        value="<?php echo $clienteEditar->celular_cli;?>">
        Ej. 099882523
      </div>
      <br>
      <br><br>
    </div>
      <button type="submit" name="button" class="btn btn-primary" style="background-color:#17BB0F">
        Guardar
      </button>
      <a href="<?php echo site_url()?>/clientes/index"  class="btn btn-danger">
        Cancelar
      </a>
      <br><br><br>
    </form>
  </div>
  <div class="col-md-3 text-center" >
  </div>
</div>
</div>
</div>
</div>
</div>
</section>


  </div>
  <div class="col-md-3 text-center" >
  </div>

</div>
<script type="text/javascript">
$("#frm_editarCliente").validate({
  rules:{
    cedula_cli:{
      required:true,
      digits:true,
      minlength:10,
      maxlength:10,
    },
    apellidos_cli:{
      required:true
    },
    nombres_cli:{
      required:true
    },
    telefono_cli:{
      digits:true,
      minlength:9,
      maxlength:9
    },
    celular_cli:{
      required:true,
      digits:true,
      minlength:10,
      maxlength:10
    },
    direccion_cli:{
      required:true
    }
  },
  // -----------------Mensajes----------
  messages:{
        cedula_cli:{
          required:"Por favor ingrese la cedula",
          digits:"Por favor ingrese solo numeros",
          minlength:"La cedula debe tener minimo 10 digitos",
          maxlength:"La cedula debe tener maximo 10 digitos"
        },
        apellidos_cli:{
          required:"Por favor ingrese el Apellido"
        },
        nombres_cli:{
          required:"Por favor ingrese los nombres"
        },
        telefono_cli:{
          digits:"Por favor ingrese el telefono ",
          minlength:"El telefono debe tener minimo 9 digitos",
          maxlength:"El telefono debe tener maximo 9 digitos"
        },
        celular_cli:{
          required:"Ingrese el numero de celura",
          digits:"Ingrese solo numeros",
          minlength:"El numero de celular debe tener minimo 10 digitos",
          maxlength:"El numero de celular debe tener maximo 10 digitax"
        },
        direccion_cli:{
          required:"Por favor ingrese la direccion"
        }
      }
    });

</script>
