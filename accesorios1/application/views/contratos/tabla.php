<center>
  <h4 ><font color="black"> ACCESORIOS DE COMPRA REGISTRADOS</font></h4>
</center>

<?php if ($listadocontratos):?>
  <table class="table table-bordered table-striped  table-hover">
    <thead>
      <tr>
        <th class="text-center">CODIGO</th>
        <th class="text-center">CLIENTE</th>
        <th class="text-center">ACCESORIO</th>
        <th class="text-center">COSTO</th>
        <th class="text-center">FECHA DE COMPRA</th>
        <th class="text-center">FECHA DE PAGO</th>
        <th class="text-center">ESTADO</th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadocontratos->result() as $contratosTemporal ): ?>
        <tr>
          <td class="text-center"><?php echo $contratosTemporal->id_con; ?></td>
          <td class="text-center"><?php echo $contratosTemporal->nombres_cli; ?></td>
          <td class="text-center"><?php echo $contratosTemporal->nombre_arti; ?></td>
          <td class="text-center"><?php echo $contratosTemporal->costo_arti; ?></td>
          <td class="text-center"><?php echo $contratosTemporal->fecha_contrato_con; ?></td>
          <td class="text-center"><?php echo $contratosTemporal->fecha_pago_con; ?></td>
          <td class="text-center"><?php echo $contratosTemporal->estado_con; ?></td>
          <td class="text-center">
            <a href="<?php echo site_url();?>/contratos/editar/<?php echo $contratosTemporal->id_con;?>"><i class='fa fa-pencil' title="Editar"></i></a>
            <!-- onclick="return confirm('Estas seguro de eliminar es para que salga un mensaje de configuracion -->
            <a href="<?php echo site_url();?>/contratos/eliminarcontrato/<?php echo $contratosTemporal->id_con;?>"
                 onclick="confirmation(event)">
                 <i class='fa fa-trash-o' title="Eliminar"></i>
            </a>
            <a href="<?php echo site_url();?>/contratos/imprimir/<?php echo $contratosTemporal->id_con;?>"
                 <i class='fa fa-print' title="Imprimir"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    No se encontraron usuarios registrados
  </div>
<?php endif; ?>
