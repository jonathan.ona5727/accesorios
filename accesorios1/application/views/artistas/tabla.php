<br><br>
<section class="breadcumb-area bg-img bg-overlay" style="background-image: url(https://images2.alphacoders.com/217/thumb-1920-21762.jpg);">
       <div class="bradcumbContent">
           <p></p>
           <h2>Lista de accesorio</h2>
       </div>

   </section>
   <center>
  <h4 style="font-weight:bold">Accesorio Registrado</h4>
</center>
<?php if ($listadoArtistas):?>
  <table class="table table-bordered table-striped  table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">FOTO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">STOK</th>
        <th class="text-center">CATEGORIA<br>ACCESORIO</th>
        <th class="text-center">PRECIO</th>
        <th class="text-center">ACCIONES</th>

      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoArtistas->result() as $artistaTemporal ): ?>
        <tr>
          <td class="text-center"><?php echo $artistaTemporal->id_arti; ?></td>
          <td class="text-center">
          <?php if ($artistaTemporal->imagen_arti!=""): ?>
            <a target="_blank"
              href="<?php echo base_url('uploads').'/'.$artistaTemporal->imagen_arti;?>">
              <img src="<?php echo base_url('uploads').'/'.$artistaTemporal->imagen_arti;?>"
              width="80px" alt="">
              </a>
            <?php else: ?>
              N/A
            <?php endif; ?>

          <td class="text-center"><?php echo $artistaTemporal->nombre_arti; ?></td>
          <td class="text-center"><?php echo $artistaTemporal->telefono_arti; ?></td>
          <td class="text-center"><?php echo $artistaTemporal->nombre_gen; ?></td>
          <td class="text-center"><?php echo $artistaTemporal->costo_arti; ?></td>
          <td class="text-center">
            <a  href="<?php echo site_url();?>/contratos/cliente/<?php echo $artistaTemporal->id_arti;?>"><i class='fa fa-handshake-o' title="Contratar"></i></a>

          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    No se encontraron aCCESORIO registrados
  </div>
<?php endif; ?>
