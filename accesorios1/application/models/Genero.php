<?php

class Genero extends CI_Model{
  //insertar datos en la tabla cliente
  public function insertar($datosGenero){
    return $this->db->insert('genero',$datosGenero);
  }
  //consulatar los datos de BBD
  public function obtenerDatos(){
  $this->db->order_by('nombre_gen','asc');///para ordenar los registros desc  o asc
    $query=$this->db->get('genero');
    if ($query->num_rows()>0) {
      return $query;
    } else{
      return false;
    }
}
// funcio para editar cliente
public function obtenerPorId($id) {
  $this->db->where("id_ge",$id);
  $query=$this->db->get('genero');
  if ($query->num_rows()>0) {
    return $query->row();
  }else {
    return false;
  }
}

// funcion para eliminar cliente
public function eliminarPorId($id_ge){
  // Definir la tabla y condiciones, usuario q queremos eliminar
  $this->db->where("id_ge",$id_ge);
  // usuario es latabla donde va a eliminar
  // devuelva el resultado true o FALSE
  return $this->db->delete("genero");
}

// funcion para procesar la actualizacion del computador
public function actualizar($id,$datosGenero){
  $this->db->where('id_ge',$id);
  return $this->db->update('genero',$datosGenero);
}

public function consultaGeneroPorNombre($nombre_gen){
  $this->db->where('nombre_gen',$nombre_gen);
  $query=$this->db->get('genero');
  if($query->num_rows()>0){
    return $query->row();
  }else{
    return false;
  }
}

}

 ?>
