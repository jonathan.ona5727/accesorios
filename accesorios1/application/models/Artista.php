<?php

class Artista extends CI_Model{
  //insertar datos en la tabla cliente
  public function insertar($datosArtista){
    return $this->db->insert('artista',$datosArtista);
  }
  //funcion para consultar datos de BDD
  public function obtenerTodos(){
    // oara ordenar los registrados
    $this->db->order_by('nombre_arti','asc');
        $query=$this->db->select('*','fk_id_ge')
                        ->from('artista')
                        ->join("genero", "genero.id_ge = artista.fk_id_ge")
                        ->get();
        if ($query->num_rows()>0) {
          return $query;//cuando si hay registros en la BDD
        }else {
          return false;//cuando no hay registros
        }
      }

// funcio para editar pelicula
public function obtenerPorId($id) {
  $this->db->where("id_arti",$id);
  $query=$this->db->get('artista');
  if ($query->num_rows()>0) {
    return $query->row();
  }else {
    return false;
  }
}

// funcion para eliminar cliente
public function eliminarPorId($id_arti){
  // Definir la tabla y condiciones, usuario q queremos eliminar
  $this->db->where("id_arti",$id_arti);
  // usuario es latabla donde va a eliminar
  // devuelva el resultado true o FALSE
  return $this->db->delete("artista");
}
// funcion para procesar la actualizacion de peliculas
public function actualizar($id,$datosArtista){
  $this->db->where('id_arti',$id);
  return $this->db->update('artista',$datosArtista);
}

public function consultaArtistaPorNombre($nombre_arti){
  $this->db->where('nombre_arti',$nombre_arti);
  $query=$this->db->get('artista');
  if($query->num_rows()>0){
    return $query->row();
  }else{
    return false;
  }
}

// telefono
public function consultaTelefono($telefono_arti){
  $this->db->where('telefono_arti',$telefono_arti);
  $query=$this->db->get('artista');
  if($query->num_rows()>0){
    return $query->row();
  }else{
    return false;
  }
}

}

 ?>
