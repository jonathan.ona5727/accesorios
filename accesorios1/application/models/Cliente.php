<?php

class Cliente extends CI_Model{
  //insertar datos en la tabla cliente
  public function insertar($datosCliente){
    return $this->db->insert('clientes',$datosCliente);
  }
  //consulatar los datos de BBD
  public function obtenerDatos(){
    $this->db->order_by('nombres_cli','asc');///para ordenar los registros desc  o asc
    $this->db->order_by('id_cli','desc');
    $query=$this->db->get('clientes');
    if ($query->num_rows()>0) {
      return $query;
    } else{
      return false;
    }
}
// funcio para editar cliente
public function obtenerPorId($id) {
  $this->db->where("id_cli",$id);
  $query=$this->db->get('clientes');
  if ($query->num_rows()>0) {
    return $query->row();
  }else {
    return false;
  }
}

// funcion para eliminar cliente
public function eliminarPorId($id_cli){
  // Definir la tabla y condiciones, usuario q queremos eliminar
  $this->db->where("id_cli",$id_cli);
  // usuario es latabla donde va a eliminar
  // devuelva el resultado true o FALSE
  return $this->db->delete("clientes");
}

// funcion para procesar la actualizacion del computador
public function actualizar($id,$datosCliente){
  $this->db->where('id_cli',$id);
  return $this->db->update('clientes',$datosCliente);
}

public function consultaClientePorCedula($cedula_cli){
  $this->db->where('cedula_cli',$cedula_cli);
  $query=$this->db->get('clienteS');
  if($query->num_rows()>0){
    return $query->row();
  }else{
    return false;
  }
}

}

 ?>
