<?php

class Contrato extends CI_Model{
  public function insertar($datoscontrato){
    return $this->db->insert('contrato',$datoscontrato);
  }


  public function obtenerTodosPorIdUsuario($id_usu){
      $this->db->where("fk_id_usu",$id_usu);//filtrando de acuerdo al usuario conectado
      $this->db->join('clientes','clientes.id_cli=contrato.fk_id_cli');
      $this->db->join('artista','artista.id_arti=contrato.fk_id_arti');

      $query=$this->db->get('contrato');
      if ($query->num_rows()>0){
        return $query; //cuando SI hay registros en la bdd
      }else {
        return false; //cuando NO hay registros en la bdd
      }
    }
  //consulatar los datos de BBD

  public function obtenerDatos(){
    $this->db->order_by('clientes.nombres_cli','asc');///para ordenar los registros desc  o asc
    $this->db->order_by('id_con','desc');
    $this->db->join('clientes','clientes.id_cli=contrato.fk_id_cli');
    $this->db->join('artista','artista.id_arti=contrato.fk_id_arti');
    $query=$this->db->get('contrato');
    if ($query->num_rows()>0) {
      return $query;
    } else{
      return false;
    }
}



public function obtenerPorId($id) {
  $this->db->where("id_con",$id);
  $query=$this->db->get('contrato');
  if ($query->num_rows()>0) {
    return $query->row();
  }else {
    return false;
  }
}

// funcion para eliminar cliente
public function eliminarPorId($id_con){
  // Definir la tabla y condiciones, usuario q queremos eliminar
  $this->db->where("id_con",$id_con);
  // usuario es latabla donde va a eliminar
  // devuelva el resultado true o FALSE
  return $this->db->delete("contrato");
}
// funcion para procesar la actualizacion del contrato
public function actualizar($id,$datoscontrato){
  $this->db->where('id_con',$id);
  return $this->db->update('contrato',$datoscontrato);
}

 }
