<?php /**
 *
 */
class Seguridades extends CI_Controller {
  //contructor de la clase
  public function __construct(){
    parent::__construct();
      $this->load->model('seguridad');
  }
  public function index(){
    // /* Cargar la viesta nuevo*/
    // /* Carpeta/archivos*/
    $this->load->view('encabezado');
    $this->load->view('seguridades/index');
    $this->load->view('pie');

  }
  public function loging(){
    $this->load->view('encabezado');
    $this->load->view('seguridades/loging');
    $this->load->view('pie');
  }
  public function guardarRegistro(){

  $email=$this->input->post('email_usu');
  $pass=$this->input->post('password_usu');
  $perfil=$this->input->post('perfil_usu');
  $estado=$this->input->post('estado_usu');

  $datosNuevoregistro= array("email_usu" =>$email,
  "password_usu"=>md5($pass),
  "perfil_usu"=>$perfil,
  "estado_usu"=>$estado);
  if ($this->seguridad->insertar($datosNuevoregistro)) {
    // MENSAJE FLASH PARA CONFIRMAR LA INSERCION DEL registro
    $this->session->set_flashdata('confirmacion','Sus datos se han guardado exitosamente');
    redirect('http://localhost/One_Music/');
  }else {
    // no se inserto
    echo "Error al insertar";
  }
}

  public function autenticarUsuario(){
    $email=$this->input->post('email_usu');
    $password=$this->input->post('password_usu');
    $usuarioConsultado=$this->seguridad->ConsultarPoEmailPassword($email,$password);
    if ($usuarioConsultado) {
      //$this->session->set_flashdata('confirmacion','Email y Contraseña correctos');
      //crear uina variable decision-> userdata
      $datosSession = array("id"=>$usuarioConsultado->id_usu,
      "email"=>$usuarioConsultado->email_usu,
      "perfil"=>$usuarioConsultado->perfil_usu );
      $this->session->set_userdata("usuario_C0nectado",$datosSession);//crear session
        $this->session->set_flashdata('confirmacion','Acceso exitoso, Bien venido al sistema');
        redirect('/');

      // code...
    } else {
      $this->session->set_flashdata('error','Email o Contraseña incorrectos');
      redirect('seguridades/loging');
    }

  }
  public function cerrarSesion(){
    $this->session->sess_destroy();//borrar todas las sesiones existentes
    redirect("seguridades/loging");
  }

  public function validarCorreoExistente() {
    $email_usu=$this->input->post('email_usu');
    $registroExistente=$this->seguridad->consultaRegistro($email_usu);
    if ($registroExistente) {
      // print_r sirve para mostrar datos de un array
      echo json_encode(FALSE);
    }else {
      echo json_encode(TRUE);
    }
  }

  }
  ?>
