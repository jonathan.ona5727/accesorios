<?php /**
 *
 */
class Generos extends CI_Controller {
  //contructor de la clase
  public function __construct(){
    //constructor
    parent::__construct();
    // cargamos modelo cliete
    $this->load->model('genero');
    //proteger las ventanas

  }
  // /* Renderizar el listado de computadoras*/
  public function index(){
    // /* Cargar la viesta index*/
    // /* Carpeta/archivos*/
    $this->load->view('encabezado');
    $this->load->view('generos/index');//pasando parametros a la vista
    $this->load->view('pie');

  }
  // /* Funcion nuevo*/
  public function nuevo(){
    // /* Cargar la viesta nuevo*/
    // /* Carpeta/archivos*/
    $this->load->view('encabezado');
    $this->load->view('generos/nuevo');
    $this->load->view('pie');

  }
  public function tablageneros(){
  $data["listadoGeneros"]=$this->genero->obtenerDatos();
  // /* Cargar la viesta nuevo*/
  $this->load->view('generos/tablageneros',$data);
}

  public function guardarGenero(){

  $NombreGenero=$this->input->post('nombre_gen');
  $datosNuevoGenero= array("nombre_gen" =>$NombreGenero );
  if ($this->genero->insertar($datosNuevoGenero)) {
    // MENSAJE FLASH PARA CONFIRMAR LA INSERCION DEL CLIENTE
    $this->session->set_flashdata('confirmacion','Genero inseretado exitosamente');
    redirect('generos/index');
  }else {
    // no se inserto
    echo "Error al insertar";
  }
}
// metodo para eliminar cliente reciviendo el parametro // Id
public function eliminarGenero($id){
  // llamamos al modelo cliente
  $this->genero->eliminarPorId($id);
  // validacion de la eliminacion si se realizo o no//
  if ($this->genero->eliminarPorId($id)) {
    // MENSAJE FLASH PARA CONFIRMAR LA INSERCION DEL CLIENTE
    $this->session->set_flashdata('confirmacion','Genero eliminado exitosamente');
    redirect("generos/index");
  }else {
    echo "Error al eliminar";
  }
  }


// funcion para renderizar al la vista editar
public function editar($id){
  $data["generoEditar"]=$this->genero->obtenerPorId($id);
  $this->load->view('encabezado');
  // Cargar la vista pasando como parametro data
  $this->load->view('generos/editar',$data);
  $this->load->view('pie');
}

// metodo para llamar a la actualizacion del imagecolordeallocate
public function actualizarGenero(){
  $id_ge=$this->input->post('id_ge');
  $datosEditados=array(
    "nombre_gen"=>$this->input->post('nombre_gen')
  );
  if ($this->genero->actualizar($id_ge,$datosEditados)) {
    // MENSAJE FLASH PARA CONFIRMAR LA ACTUALIZACION DEL CLIENTE
    $this->session->set_flashdata('confirmacion','Genero actualizado exitosamente');
    redirect('generos/index');
  } else {
    // MENSAJE de erro FLASH PARA LA ACTUALIZACION DEL CLIENTE
    $this->session->set_flashdata('confirmacion','Genero actualizado ');
  }

}

public function validarGeneroExistente() {
  $nombre_gen=$this->input->post('nombre_gen');
  $clienteExistente=$this->genero->consultaGeneroPorNombre($nombre_gen);
  if ($clienteExistente) {
    // print_r sirve para mostrar datos de un array
    echo json_encode(FALSE);
  }else {
    echo json_encode(TRUE);
  }
}

}

 ?>
