<?php
  class Gestiones extends CI_Controller{

    //Constructor de la clase (Ojo doble guion bajo __)
    public function __construct(){
      parent::__construct();
      //cargar la libreria en este controlador
      $this->load->database();
      $this->load->library('grocery_CRUD');

    }

    public function gestionClientes(){
      $clientes=new grocery_CRUD();
      $clientes->set_table('clientes'); //nombre de la bbdd
      $clientes->set_language('spanish');
      $clientes->set_theme('flexigrid');
      //$clientes->set_file
      $clientes->display_as("id_cli","CÓDIGO");
      $clientes->display_as("cedula_cli","CEDULA");
      $clientes->display_as("apellidos_cli","APELLIDO");
      $clientes->display_as("nombres_cli","NOMBRE");
      $clientes->display_as("direccion_cli","DIRECCION");
      $clientes->display_as("celular_cli","TELEFONO CELULAR");
      $clientes->set_subject("cliente");
      $clientes->columns('id_cli','cedula_cli','apellidos_cli','nombres_cli','direccion_cli','celular_cli');
      $clientes->fields('cedula_cli','apellidos_cli','nombres_cli','direccion_cli','celular_cli');
      $clientes->required_fields('cedula_cli','apellidos_cli','nombres_cli','direccion_cli','celular_cli');

      $output=$clientes->render();
      $this->load->view('encabezado');
      $this->load->view('gestiones/gestionClientes',$output);
      $this->load->view('pie');
    }




  }
 ?>
