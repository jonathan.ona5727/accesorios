<?php /**
 *
 */
class Contratos extends CI_Controller {
  //contructor de la clase
  public function __construct(){
    parent::__construct();//caragmos el contructor del padre
    // cargamos los modelos para poder seleccionar los id
    $this->load->model('cliente');
    $this->load->model('artista');
    $this->load->model('contrato');
    //proteger las ventanas
    
  }

  public function tabla(){
    $idUsuarioConectado=$this->session->userdata("usuario_C0nectado")['id'];
    $data["listadocontratos"]=$this->contrato->obtenerTodosPorIdUsuario($idUsuarioConectado);
    // $data["listadocontratos"]=$this->contrato->obtenerDatos();
    $this->load->view('contratos/tabla',$data);//pasando parametros a la vista


  }
  public function cliente(){
    $data["listadocontratos"]=$this->contrato->obtenerDatos();

    $data["listadoClientes"]=$this->cliente->obtenerDatos();
    $data["listadoartistas"]=$this->artista->obtenerTodos();
    // /* Cargar la viesta index*/
    // /* Carartita/archivos*/
    $this->load->view('encabezado');
    $this->load->view('contratos/cliente',$data);//pasando parametros a la vista
    $this->load->view('pie');

  }
  public function index(){
    $data["listadocontratos"]=$this->contrato->obtenerDatos();

    $data["listadoClientes"]=$this->cliente->obtenerDatos();
    $data["listadoartistas"]=$this->artista->obtenerTodos();
    // /* Cargar la viesta index*/
    // /* Carartita/archivos*/
    $this->load->view('encabezado');
    $this->load->view('contratos/index',$data);//pasando parametros a la vista
    $this->load->view('pie');

  }
  public function imprimir($id){
    $data["listadoClientes"]=$this->cliente->obtenerDatos();
    $data["listadoartistas"]=$this->artista->obtenerTodos();
    $data["contratoEditar"]=$this->contrato->obtenerPorId($id);

    // Cargar la vista pasando como parametro data

    $this->load->view('contratos/imprimir',$data);
  }
  public function formulario() {
  $data["listadoClientes"]=$this->cliente->obtenerDatos();
  $data["listadoartistas"]=$this->artista->obtenerTodos();
    $this->load->view('encabezado');
    $this->load->view('contratos/formulario',$data);//pasando parametros a la vista
    $this->load->view('pie');
  }
  public function listadocontratos() {
    $listadocontratos=$this->alquiler->obtenerDatos();
    if ($listadocontratos) {
      foreach ($listadocontratos->result() as $contratosTemporal) {
        echo $contratosTemporal->id_con,"<br>";
        echo $contratosTemporal->nombres_cli,"<br>";
        echo $contratosTemporal->nombre_arti,"<br>";
        echo $contratosTemporal->tarifa_con,"<br>";
        echo $contratosTemporal->fecha_contrato_con,"<br>";
        echo $contratosTemporal->fecha_pago_con,"<br>";

        // code...
      }
    } else {

      $this->session->set_flashdat('error');
      redirect('contratos/index');
      // code...
    }

  }

  // funcion para renderizar al la vista editar usuarios
  public function editar($id){
    $data["listadoClientes"]=$this->cliente->obtenerDatos();
    $data["listadoartistas"]=$this->artista->obtenerTodos();
    $data["contratoEditar"]=$this->contrato->obtenerPorId($id);
    $this->load->view('encabezado');
    // Cargar la vista pasando como parametro data
    $this->load->view('contratos/editar',$data);
    $this->load->view('pie');
  }
  public function guardarPedido() {
    $id_cliente=$this->input->post('fk_id_cli');
    $id_artista=$this->input->post('fk_id_arti');
    $Fecha_contrato=$this->input->post('fecha_contrato_con');
    $Fecha_pago=$this->input->post('fecha_pago_con');
    $estado=$this->input->post('estado_con');

    $datos=array(
    "fk_id_cli"=>$id_cliente,
    "fk_id_arti"=>$id_artista,
    "fecha_contrato_con"=>$Fecha_contrato,
    "fecha_pago_con"=>$Fecha_pago,
  "estado_con"=>$estado,
  "fk_id_usu"=>$this->session->userdata("usuario_C0nectado")['id']);
    if ($this->contrato->insertar($datos)) {
      $this->session->set_flashdata('confirmacion','contrato Registrado Exitosamente');
      redirect('artistas/tabla');
    }else {
      $this->session->set_flashdata('error','Error al procesar');
      redirect('artistas/tabla');
    }
  }

  public function guardarcontratos() {
    $id_cliente=$this->input->post('fk_id_cli');
    $id_artista=$this->input->post('fk_id_arti');
    $Fecha_contrato=$this->input->post('fecha_contrato_con');
    $Fecha_pago=$this->input->post('fecha_pago_con');
    $estado=$this->input->post('estado_con');

    $datos=array(
    "fk_id_cli"=>$id_cliente,
    "fk_id_arti"=>$id_artista,
    "fecha_contrato_con"=>$Fecha_contrato,
    "fecha_pago_con"=>$Fecha_pago,
    "estado_con"=>$estado,
    "fk_id_usu"=>$this->session->userdata("usuario_C0nectado")['id']);
    if ($this->contrato->insertar($datos)) {
      $this->session->set_flashdata('confirmacion','contrato Registrado Exitosamente');
      redirect('contratos/index');
    }else {
      $this->session->set_flashdata('error','Error al procesar');
      redirect('contratos/index');
    }
  }
  //consulatar los datos de BBD
  public function obtenerDatos(){
    $query=$this->db->get('contrato');
    if ($query->num_rows()>0) {
      return $query;
    } else{
      return false;
    }
}

  public function eliminarcontrato($id){
    // llamamos al modelo cliente
    $this->contrato->eliminarPorId($id);
    // validacion de la eliminacion si se realizo o no//
    if ($this->contrato->eliminarPorId($id)) {
      // MENSAJE FLASH PARA CONFIRMAR LA INSERCION DEL CLIENTE
      $this->session->set_flashdata('confirmacion','contrato eliminado exitosamente');
      redirect("contratos/index");
    }else {
      echo "Error al eliminar";
    }
    }
    // metodo para llamar a la actualizacion del imagecolordeallocate
    public function actualizarcontrato(){
      $id_con=$this->input->post('id_con');
      $contratoEditado=array(
        "	fk_id_cli "=>$this->input->post('fk_id_cli'),
        "fk_id_arti"=>$this->input->post('fk_id_arti'),
        "fecha_contrato_con"=>$this->input->post('fecha_contrato_con'),
        "fecha_pago_con"=>$this->input->post('fecha_pago_con'),
        "estado_con"=>$this->input->post('estado_con')

      );
      if ($this->contrato->actualizar($id_con,$contratoEditado)) {
        // MENSAJE FLASH PARA CONFIRMAR LA ACTUALIZACION DEL CLIENTE
        $this->session->set_flashdata('confirmacion','contrato actualizado exitosamente');
        redirect('contratos/index');
      } else {
        // MENSAJE de erro FLASH PARA LA ACTUALIZACION DEL CLIENTE
        $this->session->set_flashdata('confirmacion','contrato Actualizado ');
      }

    }

  }


  // $this->input->post(),
  //   "fk_id_arti"=>$this->input->post("fk_id_arti"),
  //   "fecha_contrato_con"=>$this->input->post("fecha_contrato_con"),
  //   "fecha_pago_con"=>$this->input->post("fecha_pago_con")
