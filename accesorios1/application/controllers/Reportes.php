<?php
  class Reportes extends CI_Controller{
    public function __construct(){
      parent::__construct();
        $this->load->model('cliente');
      $this->load->model('artista');

      if(!$this->session->userdata("usuarioC0nectado")){
          $this->session->set_flashdata("error","Por favor Inicie Sesion");
          redirect('seguridades/cerrarSesion');
      }else{
        if($this->session->userdata("usuarioC0nectado")["perfil"]!="ADMINISTRADOR"){
            redirect('seguridades/cerrarSesion');
        }
      }
    }
    public function index(){
      $data["artistas"]=$this->artista->obtenerTodos();

      $data["clientes"]=$this->cliente->obtenerDatos();


      $this->load->view('encabezado');
      $this->load->view('reportes/index',$data);
          $this->load->view('pie');


    }

  }
